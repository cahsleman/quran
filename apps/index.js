import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';
import { Button } from 'react-native';
import { Icon } from 'react-native-elements'

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import WelcomeScreen from './WelcomeScreen';
import SurahScreen from './SurahScreen';
import DetailScreen from './DetailScreen';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


function Home({navigation}){
	return <LoginScreen navigation={navigation}></LoginScreen>
}

function Welcome(){
	return <WelcomeScreen></WelcomeScreen>
}

function Detail({navigation}){
	return <DetailScreen navigation={navigation}></DetailScreen>
}



function Surah({navigation}){
	//return (<SurahScreen></SurahScreen>);
	// return (<Drawer.Navigator initialRouteName="Surah">
    //     <Drawer.Screen name="Surah" component={SurahScreen} navigation={navigation} />
    //     <Drawer.Screen name="About" component={AboutScreen} />
    //   </Drawer.Navigator>);
	 return (
		<Tab.Navigator initialRouteName="Surah" barStyle={{marginTop:10}}>
			<Tab.Screen name="Surah" component={SurahScreen} navigation={navigation} options={{ 
				title: 'Surah',
				tabBarLabel: 'Surah',
				tabBarIcon: ({ color, size }) => (
					<Icon name="library-books" size={30} color="#A9A9B0"/>
				),
			}}  />
			<Tab.Screen name="About" component={AboutScreen} navigation={navigation} options={{ 
				title: 'About',
				tabBarLabel: 'About',
				tabBarIcon: ({ color, size }) => (
					<Icon name="person" size={30} color="#A9A9B0"/>
				),
			}}
			 />
		</Tab.Navigator>
	 );
}

function openLayar(navigation){
	console.log(navigation);
}

function App({navigation}){
	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="Login" >
				<Stack.Screen name='Login' component={Home} options={{ headerShown: false}}  />
				<Stack.Screen name='About' component={AboutScreen} options={{ title: 'Tentang Kami' }} />
				<Stack.Screen name='Welcome' component={Welcome} options={{ title: 'Welcome' }} />
				<Stack.Screen name='Surah' component={Surah} options={{ title: 'Surat',headerShown:false}} />
				<Stack.Screen name='Detail' component={DetailScreen} options={{ title: 'Detail' }} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}

export default App

// export default class App extends React.Component {
// 	constructor(props){
// 		super(props);
// 	}

// 	openDrawer(navigation){
// 		console.log(navigation);
// 	}

//   	render() {
// 		return (
// 			<NavigationContainer>
// 				<Stack.Navigator initialRouteName="Login" >
// 				<Stack.Screen name='Login' component={Home} options={{ headerShown: false}}  />
// 				<Stack.Screen name='About' component={AboutScreen} options={{ title: 'Tentang Kami' }} />
// 					<Stack.Screen name='Welcome' component={Welcome} options={{ title: 'Welcome' }} />
// 					<Stack.Screen name='Surah' component={Surah} options={{
// 						headerTitle: "Surah",
// 						headerLeft: (navigation) => (
// 							<Icon
// 							raised
// 							name='navicon'
// 							type='font-awesome'
// 							color='#f50'
// 							navigation={navigation}
// 							onPress={({navigation}) => console.log(this.props.navigation)} />
// 						),
// 						}} />
// 					<Stack.Screen name='Detail' component={DetailScreen} options={{ title: 'Detail' }} />
// 				</Stack.Navigator>
// 			</NavigationContainer>
//     );
//   }
// }
