import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet , ImageBackground, Image, TextInput, ActivityIndicator, FlatList, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';

export default class SurahScreen extends Component {

	constructor(props) {
		super(props);
		this.state = {
		  data: {},
		  isLoading: true,
		  isError: false,
		};
	  }

	  componentDidMount() {
		this.getSurah()
	  }

	getSurah = async () => {
		try {
			const response = await Axios.get('http://api.cahsleman.com/quran/get_surah?format=jsonp');
			this.setState({ isError: false, isLoading: false, data: response.data.surah })
		  } catch (error) {
			this.setState({ isLoading: false, isError: true })
		  }
	}

	render() {

		if (this.state.isLoading) {
			return (
				<View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
					<ActivityIndicator size='large' color='red' />
				</View>
			)
		}else if (this.state.isError) {
			return (
			  <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
				<Text>Terjadi Error Saat Memuat Data</Text>
			  </View>
			)
		}

		return (
			<View style={styles.container}>
				<View style={styles.logoBox}>
					<Text style={styles.welcomeTitle}>DAFTAR SURAT</Text>
				</View>
				<View style={styles.separator}></View>
				<View style={styles.surahContainer}>

						<FlatList
							data={this.state.data}
							renderItem={({ item }) =>
								<SurahList item={{item}} navigation={this.props.navigation}></SurahList>
							}
							keyExtractor={(item, index) => {
								return item.index;
							}}
						/>

				</View>

			</View>
		);
	}

}

class SurahList extends Component {
	render(){
		return(
			<TouchableOpacity onPress={({item})=> this.props.navigation.navigate('Detail',{dataSurah:this.props.item.item}) }>
				<View style={styles.viewList}>
					<View style={{marginRight:5}}><Icon name="library-books" size={20} color="#A9A9B0"/></View>
					<View style={{alignSelf:'flex-start', paddingLeft:5}}>
						<Text style={{alignSelf:'flex-start', fontWeight:'bold', color:'#01a9b4'}}>{this.props.item.item.surat_indonesia}</Text>
						<Text style={{fontSize:13, color:'#BBBBBB', textAlign:'left'}}>{this.props.item.item.arti}</Text>
					</View>
					<View style={styles.iconChevron}><Icon name="chevron-right" size={20} color="#A9A9B0"/></View>
				</View>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1,
		backgroundColor:'white'
	},
	logoBox: {
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
		marginTop:40
	},
	welcomeTitle:{
		fontSize:18,
		marginTop: 10,
		fontWeight:'bold',
		color:'#086972',
		padding:10
	},
	separator:{
		borderBottomWidth:1,
		marginRight:30,
		marginLeft:30,
		marginTop:10,
		borderColor:'#EEEEEE'
	},
	surahContainer:{
		marginRight:20,
		marginLeft:20
	},
	viewList:{
		flexDirection:'row',
		justifyContent:'flex-start',
		borderBottomWidth:1,
		padding:20,
		paddingLeft:0,
		borderBottomColor:'#EEEEEE',
	},
	iconChevron:{
		position:'absolute',
		right:0,
		marginTop:25
	}

});