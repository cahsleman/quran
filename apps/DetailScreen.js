import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet , ImageBackground, Image, TextInput, ActivityIndicator, FlatList, Icon } from 'react-native'
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import Axios from 'axios';

export default class DetailScreen extends Component {

	constructor(props) {
		super(props);
		this.state = {
		  data: {},
		  isLoading: true,
		  isError: false,
		  detailSurah:{}
		};
	}

	componentDidMount() {
		console.log(this.props);
		this.getAyah()
	}

	getAyah = async () => {
		try {
			let dataSurah = this.props.route.params.dataSurah;
			this.setState({detailSurah:dataSurah});
			const response = await Axios.get('http://api.cahsleman.com/quran/get_ayah/'+dataSurah.index);
			this.setState({ isError: false, isLoading: false, data: response.data.ayah });
			this.props.navigation.setOptions({ title: 'Surah '+dataSurah.surat_indonesia });
		  } catch (error) {
			this.setState({ isLoading: false, isError: true })
		  }
	}

	render() {

		if (this.state.isLoading) {
			return (
				<View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
					<ActivityIndicator size='large' color='red' />
				</View>
			)
		}else if (this.state.isError) {
			return (
			  <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
				<Text>Terjadi Error Saat Memuat Data</Text>
			  </View>
			)
		}

		return (
			<View style={styles.container}>
				<View style={styles.logoBox}>
		<Text style={styles.welcomeTitle}>{this.state.detailSurah.index}. Surah {this.state.detailSurah.surat_indonesia}</Text>
					<Text style={{fontStyle:'italic'}}>{this.state.detailSurah.arti}</Text>
				</View>
				<View style={styles.separator}></View>
				<View style={styles.surahContainer}>
					<FlatList
						data={this.state.data}
						renderItem={({ item }) =>
							<DetailList item={{item}}></DetailList>
						}
						keyExtractor={(item, index) => {
							return item.ayatID;
						  }}
					/>
				</View>

			</View>
		);
	}

}

class DetailList extends Component {
	render(){
		return(
			<View style={styles.viewList}>
				<Text style={{fontStyle:'italic', marginBottom:5}}>({this.props.item.item.ayatID})</Text>
				<Text style={{writingDirection: "ltr", textAlign:'right', fontSize:30, marginBottom:10}}>{this.props.item.item.text_arab}</Text>
				<Text style={{fontStyle:'italic', marginBottom:5}}>{this.props.item.item.readText}</Text>
				<Text>{this.props.item.item.text_indo}</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1,
		backgroundColor:'white'
	},
	logoBox: {
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
	},
	welcomeTitle:{
		fontSize:18,
		marginTop: 10,
		fontWeight:'bold'
	},
	separator:{
		borderBottomWidth:1,
		marginRight:30,
		marginLeft:30,
		marginTop:10,
		borderColor:'#EEEEEE'
	},
	surahContainer:{
		marginRight:30,
		marginLeft:30
	},
	viewList:{
		borderBottomWidth:1,
		padding:20,
		paddingLeft:0,
		borderBottomColor:'#EEEEEE'
	},
	iconChevron:{
		position:'absolute',
		right:0
	}
});
