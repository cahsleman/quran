import React, { Component, useCallback  } from 'react'
import { Alert, View, Text, TouchableOpacity, StyleSheet , Image, Linking } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component {

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.logoBox}>
					<Image source={require('../assets/logo-quran.png')} style={{width:100, height:100, marginTop:50}} />
					<Text style={styles.welcomeTitle}>Welcome To Quran</Text>
				</View>
				<View style={styles.separator}></View>
				<View style={styles.welcomeCaption}>
					<Text style={{fontStyle:'italic', fontSize:11, marginBottom:10}}>Assalamu'alaikum warahmatullahi wabarakatuh</Text>
					<Text style={{fontSize:12}}>Al Quran digital dengan terjemahan Bahasa Indonesia full. Lengkap dengan 114 surah atau 30 juz tanpa pembatasan. Dengan tampilan antar muka yang friendly dan mudah digunakan. </Text>
				</View>
				<View style={styles.separator}></View>
				<View style={{alignItems:'center',flex:1, marginTop:40}}>
					<Text>Developed by :</Text>
					<Text style={{fontStyle:'italic'}}>Nanang Haris Setiyawan :</Text>
					<View style={styles.developeBy}>
						<View style={styles.socmedIcon}>
							<OpenURLButton url="instagram://user?username=nanankharies" icon="instagram" size={50}></OpenURLButton>
						</View>
						<View style={styles.socmedIcon}>
							<OpenURLButton url="fb://nanank.haries" icon="facebook" size={50}></OpenURLButton>
						</View>
						<View style={styles.socmedIcon}>
							<OpenURLButton url="twitter://nanankharies" icon="twitter" size={50}></OpenURLButton>
						</View>
					</View>
					<Text style={{fontStyle:'italic', marginTop:30}}>Dwi Suryanta</Text>
					<View style={styles.developeBy}>
						<View style={styles.socmedIcon}>
							<OpenURLButton url="instagram://user?username=esadwisuryanta" icon="instagram" size={50}></OpenURLButton>
						</View>
						<View style={styles.socmedIcon}>
							<OpenURLButton url="https://facebook.com/dsuryanta" icon="facebook" size={50}></OpenURLButton>
						</View>
					</View>
				</View>
				
			</View>
		);
	}

}

const OpenURLButton = ({ url, icon, size }) => {
	const handlePress = useCallback(async () => {
	const supported = await Linking.canOpenURL(url);

	if (supported) {
		await Linking.openURL(url);
	  } else {
		Alert.alert(`Don't know how to open this URL: ${url}`);
	  }
	}, [url]);
	
	return (<TouchableOpacity onPress={handlePress}>
			<Icon name={icon} size={size} color="#A9A9B0"/>
		</TouchableOpacity>);
	//return <Button title={children} onPress={handlePress} />;
  };

const styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'white'
	},
	logoBox: {
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
	},
	welcomeTitle:{
		fontSize:18,
		marginTop: 20,
	},
	separator:{
		borderBottomWidth:1,
		marginRight:50,
		marginLeft:50,
		marginTop:10,
		borderColor:'#EEEEEE'
	},
	welcomeCaption:{
		color:'#A9A9B0',
		padding: 50
	},
	developeBy:{
		marginTop:20,
		flexDirection:'row'
	},
	socmedIcon:{
		marginLeft:30,
		marginRight:30
	}
});