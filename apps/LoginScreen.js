import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, ImageBackground, TouchableOpacity, Image } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

export default class LoginScreen extends  React.Component {

	constructor(props) {
		super(props);

		this.state = {
			userName: '',
			password: '',
			isError: false,
			styleUsername:'',
			stylePassword:''
		  }
	  }

	  onFocusUsername() {
		this.setState({ styleUsername: styles.textinput_focused })
	  }
	  onFocusPassword() {
		this.setState({ styleUsername: styles.textinput_focused })
	  }
	  onBlurUsername() {
		this.setState({ stylePassword: styles.textinput_unfocused })
	  }
	  onBlurPassword() {
		this.setState({ stylePassword: styles.textinput_unfocused })
	  }

	  loginHandler() {
		if(this.state.password == 'asd'){
		  this.props.navigation.navigate('Surah',{navigation:this.props.navigation});
		}else{
		  this.setState({isError: true});
		}
	  }

	render() {
		return (
			<View style={ styles.container }>
				<ImageBackground  source={require('../assets/bg.jpg')} style={styles.backgroundImage} >
					<Image source={require('../assets/logo-quran.png')} style={{width:200, height:200, marginTop:100}} />
					<View style={ styles.loginForm }>
						<TextInput 
							///style={[styles.textInput, this.state.styleUsername] }
							style={styles.textInput}
							placeholder='Username'
							placeholderTextColor='white'
							onChangeText={userName => this.setState({ userName })}
							//underlineColorAndroid='transparent'
							//onBlur={ () => this.onBlurUsername() }
							//onFocus={ () => this.onFocusUsername() }
						>
						</TextInput>
						<TextInput 
							//style={[styles.textInput, this.state.stylePassword] }
							style={styles.textInput}
							placeholder='Password'
							placeholderTextColor='white'
							secureTextEntry={true}
							onChangeText={password => this.setState({ password })}
							//onBlur={ () => this.onBlurPassword() }
							//onFocus={ () => this.onFocusPassword() }
							//underlineColorAndroid='transparent'
						></TextInput>
						{/* <TouchableOpacity style={styles.buttonLogin} onPress={() => navigation.navigate('About')}> */}
						<TouchableOpacity style={styles.buttonLogin} onPress={()=>this.loginHandler()}>
							<Text style={{textAlign:'center', marginTop:5}}>LOGIN</Text>
						</TouchableOpacity>
						<Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Gunakan password "asd" untuk login </Text>
					</View>
				</ImageBackground >
			</View>

			
		);
	}

}

var styles = StyleSheet.create({
    container: {
		flex: 1,
		flexDirection:'column'
    },
    backgroundImage: {
        flex: 1,
		//resizeMode: 'cover', // or 'stretch',
		backgroundColor:'#121E23',
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
		
    },
    loginForm: {
		flex:1,
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
		
	},
	textInput:{
		borderWidth:1,
		padding:5,
		paddingLeft:20,
		borderColor:'white',
		width:300,
		marginBottom:10,
		borderRadius:50,
		color:'white'
	},
	buttonLogin: {
		borderWidth:1,
		padding:5,
		backgroundColor:'white',
		width:250,
		height:40,
		marginBottom:10,
		borderRadius:50,
		textAlign:'center'
	},
	textinput_unfocused :{
		backgroundColor:'white',
		color:'white'
	},
	textinput_focused:{
		backgroundColor:'white',
		color:'red'
	},
	errorText: {
		color: 'red',
		textAlign: 'center',
		marginBottom: 16,
	},
	hiddenErrorText: {
		color: 'transparent',
		textAlign: 'center',
		marginBottom: 16,
	}
});