import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet , ImageBackground, Image, TextInput } from 'react-native'
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

export default class WelcomeScreen extends Component {

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.logoBox}>
					<Image source={require('../assets/logo-quran.png')} style={{width:100, height:100, marginTop:50}} />
					<Text style={styles.welcomeTitle}>Welcome To Quran</Text>
				</View>
				<View style={styles.separator}></View>
				<View style={styles.welcomeCaption}>
					<Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ut mi aliquet nibh dictum facilisis. Proin feugiat odio eget elit ultrices molestie. Aliquam tincidunt aliquet dolor in rhoncus. Curabitur tempus faucibus posuere. Etiam tincidunt velit augue, a imperdiet felis pharetra in. Duis sit amet augue et turpis consectetur rhoncus. Curabitur massa magna, tristique euismod mattis non, ornare sit amet arcu. Etiam eu venenatis odio.

Morbi bibendum nulla neque, vel faucibus ipsum tincidunt ac. Etiam libero leo, pulvinar varius sodales quis, viverra at dui. Fusce fermentum imperdiet ipsum nec tristique. Curabitur nec imperdiet erat. Fusce mattis tristique pretium
</Text>
				</View>
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'white',
		height:400
	},
	logoBox: {
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center',
	},
	welcomeTitle:{
		fontSize:18,
		marginTop: 50,
	},
	separator:{
		borderBottomWidth:1,
		marginRight:50,
		marginLeft:50,
		marginTop:10,
		borderColor:'#EEEEEE'
	},
	welcomeCaption:{
		color:'#A9A9B0',
		padding: 50
	}
});